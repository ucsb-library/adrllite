ARG ALPINE_VERSION=3.20
ARG RUBY_VERSION=3.3

FROM ruby:$RUBY_VERSION-alpine$ALPINE_VERSION AS adrl-base
ARG BUILD_TIME_APKS='build-base curl gcompat git libpq libxml2-dev libxslt-dev postgresql-dev tzdata zip'
ENV RUBY_ABI=3.2.0
ENV RAILS_ROOT=/home/adrl
ENV PATH="$RAILS_ROOT/bin:${PATH}"
ENV RAILS_SERVE_STATIC_FILES=1
WORKDIR $RAILS_ROOT
RUN chmod +t /tmp
RUN apk --no-cache add $BUILD_TIME_APKS

FROM adrl-base AS adrl-dev
ARG DEV_APKS='less postgresql-client shared-mime-info'
RUN apk --no-cache add $DEV_APKS
COPY . .
RUN gem update bundler
RUN bundle install --jobs "$(nproc)"
ENTRYPOINT ["bin/docker-entrypoint.sh"]
CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]

FROM adrl-base AS adrl-prod
ENV BUNDLE_PATH="$RAILS_ROOT/vendor/bundle"
ARG GID=10001
ARG UID=10000
ARG PROD_APKS='tini'
RUN apk --no-cache add $PROD_APKS
RUN addgroup -S --gid $GID app && \
  adduser -S -G app -u $UID -s /bin/sh -h /app app
COPY . .
RUN gem update bundler
RUN bundle install --jobs "$(nproc)"
RUN chown $UID:$GID . .
RUN chmod 777 tmp
RUN bundle config set deployment 'true'
RUN bundle config set without 'test development'
ENTRYPOINT ["tini", "--", "bin/docker-entrypoint.sh"]
USER $UID
CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]
