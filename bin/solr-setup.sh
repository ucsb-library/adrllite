#!/usr/bin/env sh

while ! nc -z "$SOLR_HOST" "$SOLR_PORT"; do echo "--- Waiting for Solr ..."; sleep 5s; done

solrcloud-upload-configset.sh "solr/conf"
solrcloud-create-collection.sh
