#!/bin/sh

PGPASSWORD=$DB_PASSWORD PGHOST=$DB_HOST PGUSER=$DB_USERNAME PGDATABASE=$DB_NAME
export PGPASSWORD PGHOST PGUSER PGDATABASE

while ! db-wait.sh "$DB_HOST:$DB_PORT"; do echo "-- Waiting for PGSQL ..."; sleep 5s; done

psql -tc "SELECT 1 FROM pg_database WHERE datname = '$DB_NAME'" | grep -q 1 || createdb -e -w $DB_NAME
