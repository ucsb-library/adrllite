`adrllite`
==========

a lightweight discovery frontend replacement for
[Alexandria Digital Research Library (ADRL)][adrl].


[adrl]: https://alexandria.library.ucsb.edu
